# AyD1_Proyecto

> Universidad De San Carlos de Guatemala
> Facultad de Ingenieria
> Primer semestre del año 2021
> **Curso:** Análisis y Diseño de Sistemas 1

## Integrantes del grupo
* 201602999 Alex Rene Lopez Rosa
* 201610673 Yaiza Estefanía Pineda González
* 201504095 Virginia Cristel Mishel Medina Ramírez
* 201325641 Brayan Giovanny Rivas Estrada
* 201612139 Jeralmy Alejandra de León Samayoa
* 201612185 Byron Antonio Alvarez Morales